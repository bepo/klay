# -*- coding: utf-8 -*-

import types, os, getopt, sys, locale

from numpy import *

from klay_lib import *
from klay_typist import *

class Librarian(VirtualTypist) :
    def __init__(self, keymap, layout, bridge, charset, corpus) :
        VirtualTypist.__init__(self,keymap,layout,bridge,charset)
        self.corpus = corpus
        self.stats = {}
        self.keyGroupDefinition = { 'upper_row' : ['AD01', 'AD02', 'AD03', 'AD04', 'AD05', 'AD06', 'AD07', 'AD08', 'AD09', 'AD10', 'AD11', 'AD12'],
                                    'middle_row' : ['AC01', 'AC02', 'AC03', 'AC04', 'AC05', 'AC06', 'AC07', 'AC08', 'AC09', 'AC10', 'AC11', 'BKSL'],
                                    'lower_row' : ['LSGT', 'AB01', 'AB02', 'AB03', 'AB04', 'AB05', 'AB06', 'AB07', 'AB08', 'AB09', 'AB10'],
                                    'digit_keys' : ['AE01', 'AE02', 'AE03', 'AE04', 'AE05', 'AE06', 'AE07', 'AE08', 'AE09', 'AE10'],
                                    'numeric_row' : ['TLDE', 'AE01', 'AE02', 'AE03', 'AE04', 'AE05', 'AE06', 'AE07', 'AE08', 'AE09', 'AE10', 'AE11', 'AE12'],
                                    'strict_home' : ['AC01', 'AC02', 'AC03', 'AC04', 'AC07', 'AC08', 'AC09', 'AC10'],
                                    'alpha_keys' : ['AE01', 'AE02', 'AE03', 'AE04', 'AE05', 'AE06', 'AE07', 'AE08', 'AE09', 'AE10',
                                                    'AD01', 'AD02', 'AD03', 'AD04', 'AD05', 'AD06', 'AD07', 'AD08', 'AD09', 'AD10', 'AD11', 'AD12',
                                                    'AC01', 'AC02', 'AC03', 'AC04', 'AC05', 'AC06', 'AC07', 'AC08', 'AC09', 'AC10', 'AC11', 'BKSL',
                                                    'LSGT', 'AB01', 'AB02', 'AB03', 'AB04', 'AB05', 'AB06', 'AB07', 'AB08', 'AB09', 'AB10'],
                                    'all_but_special' : ['TLDE', 'AE01', 'AE02', 'AE03', 'AE04', 'AE05', 'AE06', 'AE07', 'AE08', 'AE09', 'AE10', 'AE11', 'AE12',
                                             'AD01', 'AD02', 'AD03', 'AD04', 'AD05', 'AD06', 'AD07', 'AD08', 'AD09', 'AD10', 'AD11', 'AD12',
                                             'AC01', 'AC02', 'AC03', 'AC04', 'AC05', 'AC06', 'AC07', 'AC08', 'AC09', 'AC10', 'AC11', 'BKSL',
                                             'LSGT', 'AB01', 'AB02', 'AB03', 'AB04', 'AB05', 'AB06', 'AB07', 'AB08', 'AB09', 'AB10', 'SPCE'],
                                    'print' : ['TLDE', 'AE01', 'AE02', 'AE03', 'AE04', 'AE05', 'AE06', 'AE07', 'AE08', 'AE09', 'AE10', 'AE11', 'AE12',
                                             'AD01', 'AD02', 'AD03', 'AD04', 'AD05', 'AD06', 'AD07', 'AD08', 'AD09', 'AD10', 'AD11', 'AD12',
                                             'AC01', 'AC02', 'AC03', 'AC04', 'AC05', 'AC06', 'AC07', 'AC08', 'AC09', 'AC10', 'AC11', 'BKSL',
                                             'LSGT', 'AB01', 'AB02', 'AB03', 'AB04', 'AB05', 'AB06', 'AB07', 'AB08', 'AB09', 'AB10'],
                                    'all' : ['TLDE', 'AE01', 'AE02', 'AE03', 'AE04', 'AE05', 'AE06', 'AE07', 'AE08', 'AE09', 'AE10', 'AE11', 'AE12',
                                             'AD01', 'AD02', 'AD03', 'AD04', 'AD05', 'AD06', 'AD07', 'AD08', 'AD09', 'AD10', 'AD11', 'AD12',
                                             'AC01', 'AC02', 'AC03', 'AC04', 'AC05', 'AC06', 'AC07', 'AC08', 'AC09', 'AC10', 'AC11', 'BKSL',
                                             'LSGT', 'AB01', 'AB02', 'AB03', 'AB04', 'AB05', 'AB06', 'AB07', 'AB08', 'AB09', 'AB10',
                                             'SPCE','RTRN','LFSH','RTSH','RALT']
                                    }
        
        self.countOnInit()

    def __call__(self) :
        self.typeCorpus()

    def countOnInit(self) :

        key_dict_len = len(self.keyGroupDefinition['all'])

        self.prev = {}

        self.stats_pressed_chr = zeros((256))
        self.stats['consec_chrs'] = zeros((256,256))
        self.stats_pressed_key = zeros((key_dict_len))
        self.stats['consec_keys'] = zeros((key_dict_len,key_dict_len))
        self.stats['same_finger_keys'] = zeros((key_dict_len,key_dict_len))
        self.stats['alternance'] = 0.0
        self.stats['energy_per_chr'] = zeros((256))
        self.stats['energy_per_key'] = zeros((key_dict_len))
        self.stats['repeating_finger'] = {}
        self.stats['left_per_finger_energy'] = [0.0,0.0,0.0,0.0,0.0]
        self.stats['right_per_finger_energy'] = [0.0,0.0,0.0,0.0,0.0]
        self.stats['left_palm_energy'] = 0.0
        self.stats['right_palm_energy'] = 0.0

    def countOnEnd(self) :

        self.stats['total_pressed_chr'] = sum(self.stats_pressed_chr)
        self.stats['total_pressed_key'] = sum(self.stats_pressed_key)
        self.stats['total_pressed_key_but_special'] = sum(self.stats_pressed_key[0:49])

        self.stats['left_fingers_energy'] = sum(self.stats['left_per_finger_energy'])
        self.stats['right_fingers_energy'] = sum(self.stats['right_per_finger_energy'])
        
        self.stats['left_hand_energy'] = self.stats['left_fingers_energy'] + self.stats['left_palm_energy']
        self.stats['right_hand_energy'] = self.stats['right_fingers_energy'] + self.stats['right_palm_energy']

        self.stats['total_energy'] = self.stats['left_hand_energy'] + self.stats['right_hand_energy']

        self.stats['total_energy_per_chr'] = sum(self.stats['energy_per_chr'])
        self.stats['total_energy_per_key'] = sum(self.stats['energy_per_key'])

        self.stats['alternance_per_key'] = self.stats['alternance'] / (self.stats['total_pressed_key_but_special']-1)


    def printXKBLayout(self, fid) :
        for key in self.layout :
            fid.write('\nkey &lt;%s&gt;\t{ [' % key)
            f = 1
            for symbol in self.layout[key] :
                if f :
                    fid.write(' %s\t' % symbol)
                    f=0
                if symbol != '' :
                    fid.write(', %s\t' % symbol)
            fid.write('] } ;')

    def printSVGLayout(self, fid, details='full', output = 'embedded') :

        if output == 'embedded' :
            namespace = 'svg:'
            header = ''
            footer = ''
        
        fid.write('\n<%ssvg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="696" height="176">' % namespace)
        fid.write('\n<%sstyle type="text/css">' % namespace)
        fid.write('\n\ttext {fill: black; font-family: SansSerif}\n\trect {fill: none; stroke:none}')
        fid.write('\n</%sstyle>' % namespace)
        fid.write('\n<%sg id="layout">' % namespace)
        fid.write('\n\t<%srect style="fill:#ffffff;fill-opacity:1" width="696" height="176" x="0" y="0"/>' % namespace)
        
        for key in self.keyGroupDefinition['print'] :
            key_pos = self.keymap[key][2:]
            if self.keymap[key][0] == 'l' :
                abs_pos = [(key_pos[0] + 35 - self.keymap[key][1] * 6) *8, (5 + key_pos[1])*8]
            else :
                abs_pos = [(key_pos[0] + 42 + self.keymap[key][1] * 6) *8, (5 + key_pos[1])*8]
            fid.write("\n\t<svg:rect style=\"fill:%s;fill-opacity:1\" width=\"47\" height=\"39\" x=\"%d\" y=\"%d\"/>" % (self.colors[self.keymap[key][1]],8+abs_pos[0],128-abs_pos[1]))
            fid.write("<!-- %s -->" % key)
            
            fid.write("\n\t<svg:text x=\"%d\" y=\"%d\">&#x%04x;</svg:text>"% (16 + abs_pos[0],161-abs_pos[1],self.printNiceLayout(self.layout[key][0])))
            if details == 'full' :
                fid.write("\n\t<svg:text x=\"%d\" y=\"%d\">&#x%04x;</svg:text>"% (16 + abs_pos[0],145-abs_pos[1],self.printNiceLayout(self.layout[key][1])))
                fid.write("\n\t<svg:text x=\"%d\" y=\"%d\">&#x%04x;</svg:text>"% (34 + abs_pos[0],161-abs_pos[1],self.printNiceLayout(self.layout[key][2])))
                fid.write("\n\t<svg:text x=\"%d\" y=\"%d\">&#x%04x;</svg:text>"% (34 + abs_pos[0],145-abs_pos[1],self.printNiceLayout(self.layout[key][3])))
            
        fid.write("\n</svg:g>")
        fid.write("\n</svg:svg>")

    def prepareStatistics(self, key) :
        self.corpus_charset = 'iso-8859-15'
        
        if key == 'pressed_chr' :
            
            pressed_chr = self.stats_pressed_chr[:]
            energy_per_chr = self.stats['energy_per_chr']

            answer = {}
            answer['total_energy'] = sum(energy_per_chr)
            while sum(pressed_chr) :
                i = pressed_chr.argmax()
                answer['energy'] = energy_per_chr[i]
                
                if answer['energy'] != 0.0 :
                    answer['code'] = i
                    answer['count'] = int(pressed_chr[i])
                    answer['chr'] = self.printNiceHtml(i)
                    yield answer
                    
                pressed_chr[i] = 0

        elif key == 'pressed_key' :

            key_dict = self.keyGroupDefinition['all']
            pressed_key = self.stats_pressed_key.copy()
            energy_per_key = self.stats['energy_per_key']

            answer = {}
            answer['total_energy'] = sum(energy_per_key)
            while sum(pressed_key) :
                i = pressed_key.argmax()
                answer['energy'] = energy_per_key[i]
                
                if answer['energy'] != 0.0 :
                    answer['code'] = key_dict[i]
                    answer['count'] = int(pressed_key[i])
                    if key_dict[i] in self.layout :
                        answer['key'] = self.layout[key_dict[i]][0]
                    else :
                        answer['key'] = ''
                    yield answer
                pressed_key[i] = 0

        elif key == 'repeating_finger' :
            repeating_finger = self.stats[key].copy()
            answer = {}
            for k,v in sorted(self.stats[key].iteritems(), key=operator.itemgetter(1), reverse = True) :
                answer['sequence'] = self.printNiceXkb(k[0]) + " -&gt; " + self.printNiceXkb(k[1])
                answer['count'] = v
                if v > 1 :
                    yield answer


    def printReport(self, fid, params = 'hflied') :

        self.colors = { 1 : '#ff8888', 2 : '#8fff88', 3 : '#8895ff', 4 : '#ff88f6' }
        
        self.corpus.seek(0)

        if 'h' in params :
            fid.write('<?xml version="1.0" encoding="UTF-8"?>')
            fid.write('\n<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">')
            fid.write('\n<html xmlns="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">')
            fid.write('\n<head>')
            fid.write('\n\t<title>REPORT</title>')
            fid.write("""\n\t<style>
        table{border-collapse:collapse}td{border:none; padding:0.1em 0.8em}
        .grid td {border:none; padding:0.3em 0.3em}
        .grid {border:1px solid;}
                .microgrid td {border:none; padding:0.2em 0.2em;}
        .microgrid {border:1px solid;}
    </style>""")
            fid.write('\n</head>')
            fid.write('<body>')

        if 'l' in params :
            fid.write('<h1>Layout</h1>')
            self.printSVGLayout(fid, output = 'embedded')

        if 'i' in params :
            fid.write('\n<h1>Informations générales</h1>')
            fid.write('\n<ul>')
            fid.write('\n\t<li><strong>Nom du corpus : </strong><em>%s</em></li>' % self.corpus.name)
            fid.write('\n\t<p><em>«&nbsp;%s...&nbsp;»</em></p>' % unicode(self.corpus.read(128),'iso-8859-15').encode('utf-8'))
            fid.write('\n\t<li><strong>Nombre de caractères : </strong><em>%d</em></li>' % self.stats['total_pressed_chr'])
            fid.write('\n\t<li><strong>nombre de touches pressées : </strong><em>%d</em></li>' % self.stats['total_pressed_key_but_special'])
            fid.write('\n\t<ul>\n\t\t<li>en incluant les touches combinantes : <em>%d</em></li>\n\t</ul>' % self.stats['total_pressed_key'])
            fid.write('\n\t<li><strong>taux d\'alternance des mains : </strong>%0.5f</li>' % self.stats['alternance_per_key'])
            fid.write('\n</ul>')

        if 'e' in params :
            fid.write('\n<h1>Estimations des dépenses énergétiques</h1>')
            fid.write('\n<ul>')
            # fid.write('\n\t<li><strong>Version de l\'algorithme : </strong><em>%s</em></li>' % VersionNumber)
            fid.write('\n\t<li><strong>dépense Totale : </strong><em>%0.1f</em></li>' % self.stats['total_energy'])
            fid.write('\n\t<p>\n\t<table>\n\t\t<tr>\n\t\t\t<th /><th>Main Gauche</th><th>Main Droite</th>\n\t\t</tr>')
            fid.write('\n\t\t<tr style="background-color:#9F9">\n\t\t\t<td>Paume</td><td>%0.1f</td><td>%0.1f</td>\n\t\t</tr>' % (self.stats['left_palm_energy'],self.stats['right_palm_energy']))
            fid.write('\n\t\t<tr style="background-color:#99F">\n\t\t\t<td>Doigts</td><td>%0.1f</td><td>%0.1f</td>\n\t\t</tr>' % (self.stats['left_fingers_energy'],self.stats['right_fingers_energy']))
            fid.write('\n\t\t<tr style="background-color:#77F">\n\t\t\t<td><small>index</small></td><td>%0.1f</td><td>%0.1f</td>\n\t\t</tr>' % (self.stats['left_per_finger_energy'][1],self.stats['right_per_finger_energy'][1]))
            fid.write('\n\t\t<tr style="background-color:#77F">\n\t\t\t<td><small>majeur</small></td><td>%0.1f</td><td>%0.1f</td>\n\t\t</tr>' % (self.stats['left_per_finger_energy'][2],self.stats['right_per_finger_energy'][2]))
            fid.write('\n\t\t<tr style="background-color:#77F">\n\t\t\t<td><small>annulaire</small></td><td>%0.1f</td><td>%0.1f</td>\n\t\t</tr>' % (self.stats['left_per_finger_energy'][3],self.stats['right_per_finger_energy'][3]))
            fid.write('\n\t\t<tr style="background-color:#77F">\n\t\t\t<td><small>auriculaire</small></td><td>%0.1f</td><td>%0.1f</td>\n\t\t</tr>' % (self.stats['left_per_finger_energy'][4],self.stats['right_per_finger_energy'][4]))
            fid.write('\n\t\t<tr style="background-color:#F99">\n\t\t\t<th>Total</th><td><strong>%0.1f</strong></td><td><strong>%0.1f</strong></td>\n\t\t</tr>' % (self.stats['left_hand_energy'],self.stats['right_hand_energy']))
            fid.write('\n\t</table>\n\t</p>')
            fid.write('\n</ul>')

        if 'd' in params :

            fid.write('\n<h1>Statistiques détaillées</h1>')
            fid.write('\n<h2>Statistiques par caractères</h2>')
            fid.write('\n<table>\n\t<tr>\n\t\t<th>ASCII</th>\n\t\t<th>Décompte</th>\n\t\t<th>Energie</th>\n\t\t<th>%</th>\n\t\t<th>E. / car.</th>\n\t\t<th>Commentaire</th>\n\t</tr>')

            n = 0
            for s in self.prepareStatistics('pressed_chr') :
                if n % 2 :
                    color = '#8895ff'
                else :
                    color = '#ffffff'
                fid.write('\n\t<tr style="background-color:%s">' % color)
                fid.write('\n\t\t<td>0x%02x</td>' % s['code'])
                fid.write('\n\t\t<td>%d</td>' % s['count'])
                fid.write('\n\t\t<td>%0.1f</td>' % s['energy'])
                fid.write('\n\t\t<td>%0.2f</td>' % (s['energy'] / s['total_energy'] * 100))
                fid.write('\n\t\t<td>%0.1f</td>' % (s['energy'] / s['count']))
                fid.write('\n\t\t<td>%s</td>' % s['chr'])
                fid.write('\n\t</tr>')
                n +=1
            fid.write('\n</table>')

            fid.write('\n<h2>Statistiques par touches</h2>')
            fid.write('\n<table>\n\t<tr>\n\t\t<th>XKB</th>\n\t\t<th>Décompte</th>\n\t\t<th>Energie</th>\n\t\t<th>%</th>\n\t\t<th>E. / tou.</th>\n\t\t<th>Commentaire</th>\n\t</tr>')

            n = 0
            for s in self.prepareStatistics('pressed_key') :
                if n % 2 :
                    color = '#8895ff'
                else :
                    color = '#ffffff'
                fid.write('\n\t<tr style="background-color:%s">' % color)
                fid.write('\n\t\t<td>%s</td>' % s['code'])
                fid.write('\n\t\t<td>%d</td>' % s['count'])
                fid.write('\n\t\t<td>%0.1f</td>' % s['energy'])
                fid.write('\n\t\t<td>%0.2f</td>' % (s['energy'] / s['total_energy'] * 100))
                fid.write('\n\t\t<td>%0.1f</td>' % (s['energy'] / s['count']))
                fid.write('\n\t\t<td>%s</td>' % s['key'])
                fid.write('\n\t</tr>')
                n += 1
            fid.write('\n</table>')

            fid.write('\n<h2>Table de fréquence des touches consécutives</h2>')

            max_consec_keys = self.stats['consec_keys'].max()

            h = self.stats['consec_keys']
            h = vstack((h[:47,:],h[49:,:]))
            h = hstack((h[:,:47],h[:,49:]))

            max_consec_keys_reduced = h.max()

            total_consec_keys_reduced = sum(sum(h))

            fid.write('\n<table class="grid">')
            row, col = shape(self.stats['consec_keys'])
            for r in range(row) :
                if sum(self.stats['consec_keys'][r,:],0) <> 0 :
                    fid.write('\n\t<tr>')
                    for c in range(col) :
                        if sum(self.stats['consec_keys'][:,c],0) <> 0 :
                            fid.write('\n\t\t<td ')
                            if r == 48 or c == 48 :
                                i = (255 - int(float(self.stats['consec_keys'][r][c]) * 255 / max_consec_keys))
                                style_str = 'style="background-color:#%02x%02xFF"' % (i,i)
                                percent_str = ''
                            else :
                                i = (255 - int(float(self.stats['consec_keys'][r][c]) * 255 / max_consec_keys_reduced))
                                style_str = 'style="background-color:#FF%02x%02x"' % (i,i)
                                percent_str = ' : %0.2f %%' % (100 * self.stats['consec_keys'][r][c]/ total_consec_keys_reduced)
                            if i < 255 :
                                p_key = self.printNiceXkb(self.keyGroupDefinition['all'][r])
                                n_key = self.printNiceXkb(self.keyGroupDefinition['all'][c])
                                fid.write('title="%s -&gt; %s%s" %s' % (p_key,n_key, percent_str,style_str))
                            fid.write('></td>')
                    fid.write('\n\t</tr>')
            fid.write('\n</table>')

            fid.write('\n<h2>Table de fréquence des touches consécutives pressées avec le même doigt</h2>')
            fid.write('\n<table>')
            for s in self.prepareStatistics('repeating_finger') :
                if n % 2 :
                    color = '#8895ff'
                else :
                    color = '#ffffff'
                fid.write('\n\t<tr style="background-color:%s">' % color)
                fid.write('\n\t\t<td>%s</td>' % s['sequence'])
                fid.write('\n\t\t<td>%d</td>' % s['count'])
                fid.write('\n\t</tr>')
                n += 1
            fid.write('\n</table>')

            fid.write('\n<h2>Table de fréquence des caractères consécutifs</h2>')

            max_consec_chrs = self.stats['consec_chrs'].max()

            h = self.stats['consec_chrs']
            h = vstack((h[:31,:],h[33:,:]))
            h = hstack((h[:,:31],h[:,33:]))

            max_consec_chrs_reduced = h.max()

            total_consec_chrs_reduced = sum(sum(h))

            fid.write('\n<table class="microgrid">')
            row, col = shape(self.stats['consec_chrs'])
            for r in range(row) :
                if sum(self.stats['consec_chrs'][r,:],0) <> 0 :
                    fid.write('\n\t<tr>')
                    for c in range(col) :
                        if sum(self.stats['consec_chrs'][:,c],0) <> 0 :
                            fid.write('\n\t\t<td ')
                            if r == 32 or c == 32 :
                                i = (255 - int(float(self.stats['consec_chrs'][r][c]) * 255 / max_consec_chrs))
                                style_str = 'style="background-color:#%02x%02xFF"' % (i,i)
                                percent_str = ''
                            else :
                                i = (255 - int(float(self.stats['consec_chrs'][r][c]) * 255 / max_consec_chrs_reduced))
                                style_str = 'style="background-color:#FF%02x%02x"' % (i,i)
                                percent_str = ' : %0.2f %%' % (100 * self.stats['consec_chrs'][r][c]/ total_consec_chrs_reduced)
                            if i < 255 :
                                p_key = self.printNiceHtml(r)
                                n_key = self.printNiceHtml(c)
                                fid.write('title="%s (%02x) -> %s (%02x)%s" %s' % (p_key,r,n_key,c,percent_str,style_str))
                            fid.write('></td>')
                    fid.write('\n\t</tr>')
            fid.write('\n</table>')
                
                

        if 'f' in params :
            fid.write('\n</body></html>')
            
    def printNiceChr(self,chrIn) :
        if (chrIn < 0x20) or (chrIn >= 0x7f and chrIn <= 0x9f) :
            return 0x20
        else :
            return chrIn

    def printNiceXkb(self, codeIn) :
        if codeIn in self.layout :
            return self.layout[codeIn][0]
        else :
            return codeIn

    def printNiceHtml(self, chrIn) :
        if (chrIn < 0x20) or (chrIn >= 0x7f and chrIn <= 0x9f) :
            return ''
        else :
            chrIn = chr(chrIn)
            if chrIn == '<' :
                return '&lt;'
            elif chrIn == '>' :
                return '&gt;'
            elif chrIn == '"' :
                return '&quot;'
            elif chrIn == '&' :
                return '&amp;'
            else :
                return (unicode(chrIn,self.corpus_charset).encode('utf-8'))
        

    def printNiceCharacter(self,chrIn) :
        if (chrIn < 0x20) or (chrIn >= 0x7f and chrIn <= 0x9f) :
            return ''
        else :
            return (unicode(chr(chrIn),'iso-8859-15').encode('utf-8'))
        
    def printNiceLayout(self,strIn) :
        global xkbSymbolToUnicode
        if strIn in xkbSymbolToUnicode :
            return xkbSymbolToUnicode[strIn]
        else :
            return 0x0020
        pass
        
    def countOnChr(self,chrIn) :
        
        self.stats_pressed_chr[chrIn] += 1
        
        if 'chr' in self.prev :
            self.stats['consec_chrs'][self.prev['chr'],chrIn] += 1
        self.prev['chr'] = chrIn

        # DEBUG        print chr(chrIn)

    def countOnKey(self, keyIn, chrIn) :

        keyInXkbCode = self.keyGroupDefinition['all'].index(keyIn)

        self.stats_pressed_key[keyInXkbCode] +=1
        
        if self.hand == 'r' :
            self.stats['right_per_finger_energy'][self.finger] += self.rightHand.fingerEnergy
            self.stats['right_palm_energy'] += self.rightHand.palmEnergy
            self.stats['energy_per_key'][keyInXkbCode] += (self.rightHand.palmEnergy + self.rightHand.fingerEnergy)
            self.stats['energy_per_chr'][chrIn] += (self.rightHand.fingerEnergy + self.rightHand.palmEnergy)
        if self.hand == 'l' :
            self.stats['left_per_finger_energy'][self.finger] += self.leftHand.fingerEnergy
            self.stats['left_palm_energy'] += self.leftHand.palmEnergy
            self.stats['energy_per_key'][keyInXkbCode] += (self.leftHand.palmEnergy + self.leftHand.fingerEnergy)
            self.stats['energy_per_chr'][chrIn] += (self.leftHand.fingerEnergy + self.leftHand.palmEnergy)

        if keyIn in self.keyGroupDefinition['all_but_special'] :

            if ('key' in self.prev) and ('hand' in self.prev) and ('finger' in self.prev) :

                # print self.keyGroupDefinition['all'].index(self.prev_key),keyInXkbCode

                self.stats['consec_keys'][self.keyGroupDefinition['all'].index(self.prev['key']),keyInXkbCode] += 1

                # print self.stats['consec_keys'][self.keyGroupDefinition['all'].index(self.prev_key),keyInXkbCode]

                if self.prev['hand'] != self.hand :
                    self.stats['alternance'] +=1
                elif (self.prev['key'] != keyIn) and self.prev['finger'] == self.finger :
                    a = (self.prev['key'],keyIn)
                    if a in self.stats['repeating_finger'] :
                        self.stats['repeating_finger'][a] += 1
                    else :
                        self.stats['repeating_finger'][a] = 1
                    # DEBUG # print "\taltern here"
            self.prev['hand'] = self.hand
            self.prev['finger'] = self.finger
            self.prev['key'] = keyIn
            
        # DEBUG         print "\t", keyIn, "hand : %s - finger : %s (%s)" % (self.hand, self.finger, str(self.keymap[keyIn][2:]))
        # DEBUG         print "\tleft : %0.1f %0.1f - right ! %0.1f %0.1f" % (self.leftHand.palmEnergy, self.leftHand.fingerEnergy, self.rightHand.palmEnergy, self.rightHand.fingerEnergy)

    def typeCorpus(self) :
        self.corpus.seek(0)
        for c in self.readCorpus() :
            self.typeChr(c)

    def typeChr(self,chrIn) :
        self.countOnChr(chrIn)
        for key in self.yieldKeyFromByte(chrIn) :
            self.pressKey(key)
            self.countOnKey(key,chrIn)
        self.countOnEnd()
                
    def readCorpus(self) :
        c = self.corpus.read(1)
        while c :
            yield ord(c)
            c = self.corpus.read(1)
