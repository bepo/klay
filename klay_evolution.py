# coding=utf-8

import types, os, getopt, sys, locale

from numpy import *

from klay_lib import *
from klay_typist import *
from klay_scribe import *


class evolution() :
    def __init__(self,fid_xkbConf,fid_corpus) :
        self.turn = 0
        self.mutation = 0

        self.fatherLayout = parseXkbConfig(fid_xkbConf)
        self.sonLayout = parseXkbConfig(fid_xkbConf)

        self.fatherAptitude = {}
        self.sonAptitude = {}

        self.monkey = Librarian(classic_keyboard, self.sonLayout, xkb_unicode_bridge, charset_latin9, fid_corpus)
        self.monkey.typeCorpus()

        # print self.monkey.stats
        for key in self.monkey.stats :
            self.fatherAptitude[key] = self.monkey.stats[key]
            self.sonAptitude[key] = self.monkey.stats[key]

    def newGeneration(self,fid_corpus, op  ='f'):

        if op == 'f' :
            # make an archival copy of the layout
            # the previous son become the current father
            # the aptitudes results are copied too
            for key in self.sonLayout :
                self.fatherLayout[key] = self.sonLayout[key]
            for key in self.sonAptitude :
                self.fatherAptitude[key] = self.sonAptitude[key]
        else :
            # recall the caracteristics of the father
            # in order to generate a new breed (a brother)
            for key in self.fatherLayout :
                self.sonLayout[key] = self.fatherLayout[key]

        self.mutagenAgent(self.sonLayout)

        self.monkey = Librarian(classic_keyboard, self.sonLayout, xkb_unicode_bridge, charset_latin9, fid_corpus)
        
        self.monkey.typeCorpus()

        for key in self.monkey.stats :
            self.sonAptitude[key] = self.monkey.stats[key]


    def tryGeneration(self,trial = 'total_energy') :
        alternance = (self.fatherAptitude['alternance_per_key'] - self.sonAptitude['alternance_per_key']) / self.fatherAptitude['alternance_per_key']
        energy = (self.fatherAptitude['total_energy_per_chr'] - self.sonAptitude['total_energy_per_key']) / self.fatherAptitude['total_energy_per_chr']
        # print alternance, energy
        if trial == 'total_energy' :
            if self.sonAptitude['total_energy'] < self.fatherAptitude['total_energy'] :
                return 'f'
            else :
                return 'r'
        #if trial == 'adv_triptic' :
        

    def mutagenAgent(self,layoutIn) :
        xkbPool = ['AD01', 'AD02', 'AD03', 'AD04', 'AD05', 'AD06', 'AD07', 'AD08', 'AD09', 'AD10', 'AD11', 'AD12',
                   'AC01', 'AC02', 'AC03', 'AC04', 'AC05', 'AC06', 'AC07', 'AC08', 'AC09', 'AC10', 'AC11', 'BKSL',
                   'LSGT', 'AB01', 'AB02', 'AB03', 'AB04', 'AB05', 'AB06', 'AB07', 'AB08', 'AB09', 'AB10']
        
        for i in range(int(abs(random.randn())*1.5 + 1)) :
            first = int(random.randint(len(xkbPool)))
            second = first
            while first == second :
                second = int(random.randint(len(xkbPool)))
            layoutIn[xkbPool[second]], layoutIn[xkbPool[first]] = layoutIn[xkbPool[first]], layoutIn[xkbPool[second]]
                                           
        # DEBUG
        print xkbPool[first], "<->", xkbPool[second]
