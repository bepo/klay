# coding=utf-8

import types, os, getopt, sys, locale

from numpy import *

from klay_lib import *
from klay_typist import *
from klay_scribe import *
from klay_evolution import *

# locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

VersionNumber = "0.8.7"

def usage(debug_notice = ""):
    print """
    Klay calcule l'énergie nécessaire à la frappe d'un texte donné
    sur un clavier donné, décrit par un listing de configuration de type xkb.
    (Le tout est pensé pour être hautement configurable, même si cette
    première version n'est qu'on prototype poussif)

    SYNOPSIS
    python klay [options] --layout="layout_xx.xkb" --corpus="corpus_sample.txt"

    OPTIONS
    -l, --layout
                xkb-config-file est l'extrait d'un
                fichier de configuration de type xkb

    -c, --corpus
                un fichier texte de référence, encodé
                en latin9 (par défaut : "corpus.txt")
        
    -h, --help
                affiche cette aide
        
    --version
    """
    print debug_notice

DEBUG = False

if DEBUG == True :
    print "debug mode"
    corpus_file = "corpus.txt"
    xkb_file = "layout_63.xkb"
    verboseFlag = 1
else :
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hvl:c:eo:", ["help", "verbose", "layout=", "corpus=", "evolution", "output=", "version"])
    except getopt.GetoptError:
        # print help information and exit:
        usage("error while parsing arguments")
        sys.exit(2)

    # default modes
    evolution_mode = False
    corpus_file = r"corpus.txt"
    xkb_file = "layout_63.xkb"
    report_file = "report.xhtml"
    
    verboseFlag = 0
    for o, a in opts:
        # DEBUG # print o, a
        if o in ("--version"):
            print VersionNumber
            sys.exit()
        if o in ("-h", "--help"):
            usage("help requested via --help")
            sys.exit()
        if o in ("-l", "--layout"):
            xkb_file = a
        if o in ("-c", "--corpus"):
            corpus_file = a
        if o in ("-o", "--output"):
            report_file = a
        if o in ("-v", "--verbose"):
            verboseFlag = 1
        if o in ("-e", "--evolution"):
            generation_nbr = int(a)
            evolution_mode = True


if verboseFlag :
    print "ouverture de la disposition : ", xkb_file            
try:
    fid_xkbConf = open(xkb_file,'r')
except:
    usage("failed to open layout %s" % xkb_file)
    print "error !!"
    sys.exit()

if verboseFlag :
    print "ouverture du corpus : ", corpus_file            
try:
    fid_corpus = open(corpus_file,'r')
except:
    usage("failed to open corpus %s" % corpus_file)
    print "error !!"
    sys.exit()

if verboseFlag :
    print "Corpus analysé : ", corpus_file
    print "Fichier de configuration : ", xkb_file


if evolution_mode == False :
    original_layout = parseXkbConfig(fid_xkbConf)
    monkey = Librarian(classic_keyboard, original_layout, xkb_unicode_bridge, charset_latin9, fid_corpus)
    monkey.typeCorpus()
    fid_report = open(report_file,'w')

    print "printing report..."
    
    monkey.printReport(fid_report)
    fid_report.close()

    print "done !"

elif evolution_mode == True :
    fid_report = open(report_file,'w')
    timeMachine = evolution(fid_xkbConf,fid_corpus)
    timeMachine.monkey.printReport(fid_report,'h')
    m=0
    g = 'r'
    for i in range(1,generation_nbr+1) :
        print "Tour : %d - Mutation : %d" % (i, m)
        timeMachine.newGeneration(fid_corpus,g)
        g = timeMachine.tryGeneration()
        if g == 'f' :
            m += 1
            fid_report.write('\n<h2>Tour : %d - Mutation : %d</h2>' % (i, m))
            timeMachine.monkey.printReport(fid_report,'l')
            fid_report.write("\nEnergie Totale : %0.1f" % timeMachine.monkey.stats['total_energy'])
            
    fid_report.write("\n<pre>")
    timeMachine.monkey.printXKBLayout(fid_report)
    fid_report.write("\n</pre>")
    timeMachine.monkey.printReport(fid_report,'f')
    




#monkey.printReport(fid_report)

#timeMachine = evolution(fid_xkbConf)
# timeMachine.newGeneration('f')


"""
turn = 0
mutations = 0

current_layout = parseXkbConfig(fid_xkbConf)
previous_layout = parseXkbConfig(fid_xkbConf)

monkey = Librarian(classic_keyboard, current_layout, xkb_unicode_bridge, charset_latin9, fid_corpus)
monkey.typeCorpus()
fid_report = open("report_gen.xhtml",'w')
monkey.printReportHeader(fid_report)
#fid_report.write('<h2>Tour : %d - Mutation : %d</h2>' % (turn, mutations))
#monkey.printSVGLayout(fid_report)

current_alternance =  (monkey.stats['left_palm_energy'] + sum(monkey.stats['left_per_finger_energy']) + monkey.stats['right_palm_energy'] + sum(monkey.stats['right_per_finger_energy']))
#fid_report.write('energie totale : %0.1f' % current_alternance)
fid_report.close()
previous_alternance = current_alternance
mutagenAgent(previous_layout, current_layout)
for i in range(1,9999) :
    print i
    monkey = Librarian(classic_keyboard, current_layout, xkb_unicode_bridge, charset_latin9, fid_corpus)
    monkey.typeCorpus()
    current_alternance = (monkey.stats['left_palm_energy'] + sum(monkey.stats['left_per_finger_energy']) + monkey.stats['right_palm_energy'] + sum(monkey.stats['right_per_finger_energy']))
    print current_alternance

    if current_alternance < previous_alternance :
        # DEBUG     print "coin"
        fid_report = open("report_gen.xhtml",'a')
        fid_report.write('<h2>Tour : %d - Mutation : %d</h2>' % (turn, mutations))
        monkey.printSVGLayout(fid_report)
        #monkey.printReport(fid_report,details="layout_only")
        fid_report.write('energie totale : %0.1f' % current_alternance)
        fid_report.close()
        previous_alternance = current_alternance
        mutagenAgent(previous_layout, current_layout, 'f')
        mutations += 1
    else :
        #DEBUG      print "pas coin"
        mutagenAgent(previous_layout, current_layout, 'r')
    turn += 1

fid_report = open("report_gen.xhtml",'a')
monkey.printReportFooter(fid_report)
fid_report.close()
"""

#monkey.printLayout(fid_svg)
