# coding=utf-8

import types, os, getopt, sys, locale, operator
from numpy import *

class VirtualHand :
	def __init__(self) :
		self.palmStiffness = 3
		self.fingersStiffness = array([1,0.9,1,1.2,1.1])
	
		
                self.fingersPos = array([[0,0],[0,0],[0,0],[0,0],[0,0]])
                self.palmPos = array([0.0,0.0])
		self.perFingerTotalEnergy = array([0,0,0,0,0])
		
		self.palmEnergy = 0.0
		self.fingerEnergy = 0.0
		self.resetFingers()
		
	def resetFingers(self):
		# pouce, index, majeur, annulaire, auriculaire
		self.fingersPos *= 0
		self.fingerEnergy *= 0
		self.palmPos *= 0
		self.lastMovedFinger = 0

	def resetHand(self):
		self.palmPos = array([0.0,0.0])
		self.palmEnergy = 0
	
	def moveFinger(self, finger, newFingerPos) :
		
		prevFingerPos = self.fingersPos[finger]
		
		# mass-spring-like formula for the energy of a move
		self.fingerEnergy = ((newFingerPos[0] - prevFingerPos[0])**2 + (newFingerPos[1] - prevFingerPos[1])**2) ** 0.6
		self.fingerEnergy *= 0.5 * self.fingersStiffness[finger]
		
		# energy for pressing down the key
		self.fingerEnergy += 0.5
		
		# if the same finger is moved twice in a row, apply a penalty
		if self.lastMovedFinger == finger :
			self.fingerEnergy *= 1.5
		
		self.fingersPos[finger] = newFingerPos
		
		prevPalmPos = self.palmPos
		
		newPalmPos = array([sum(self.fingersPos[:,0] * self.fingersStiffness) / 5.0,
							sum(self.fingersPos[:,1] * self.fingersStiffness) / 5.0])
		
		self.palmEnergy = ((newPalmPos[0] - prevPalmPos[0])**2 + (newPalmPos[1] - prevPalmPos[1])**2) **0.6
		self.palmEnergy *= 0.5 * self.palmStiffness
		
		self.palmPos = newPalmPos
		
		self.lastMovedFinger = finger
	
class VirtualTypist :
	def __init__ (self, keymap, layout, bridge, charset) :
		self.keymap = keymap
		self.layout = layout
		self.bridge = bridge
		self.charset = charset
		self.leftHand = VirtualHand()
		self.rightHand = VirtualHand()

		self.prepareSymbols()

		self.prepareByteToKey()

	def prepareSymbols(self) :
		self.symbols = {}
		for xkbCode in self.layout :
			column = 0
			for symbol in self.layout[xkbCode] :
				if column == 0:
					# direct access key
					keySequence = xkbCode
				elif column == 1:
					# shift access key
					# check if the key is on the left or right side of the keyboard
					# and determine the appropriate shift key to be pressed
					if self.keymap[xkbCode][0] == 'l':
						keySequence = ('RTSH',xkbCode)
					else:
						keySequence = ('LFSH',xkbCode)
				elif column == 2:
					# alt-GR access key
					keySequence = ('RALT',xkbCode)
				elif column == 3:
					# shift + alt-GR access key
					keySequence = ('RALT','RTSH', xkbCode)
					
				if symbol in self.symbols:
					self.symbols[symbol].append(keySequence)
				else:
					self.symbols[symbol] = [keySequence]
					
				column += 1
	
	def prepareByteToKey(self) :		
		# prepare the dictionnary holding the sequence of pressed keys for every bytes
		self.byteToKey = {}
		for byte in range(1,256):
			keySequence = []
			for symbol in self.yieldSymbolSequence(byte):
				for key in self.yieldKeySequence(symbol):
					keySequence.append(key)
			self.byteToKey[byte] = keySequence

	def yieldSymbolSequence(self,chrIn) :
	
		unicodeLabel = self.charset[chrIn]
		
		tokenAltern = False
		
		if unicodeLabel in self.bridge:
			for symbolSequence in self.bridge[unicodeLabel]:
				if not tokenAltern :
					if type(symbolSequence)==types.TupleType :
						tokenSequence = True
						for symbol in symbolSequence :
							if symbol not in self.symbols :
								tokenSequence = False
						if tokenSequence :
							for symbol in symbolSequence:
								tokenAltern = True
								yield symbol
					else :
						if symbolSequence in self.symbols :
							tokenAltern = True
							yield symbolSequence

							
	def yieldKeySequence(self,labelIn) :
		
		tokenAltern = False
		
		for keySequence in self.symbols[labelIn]:
			if not tokenAltern:
				if type(keySequence)==types.TupleType:
					tokenSequence = True
					if tokenSequence:
						for key in keySequence:
							tokenAltern = True
							yield key
				else:
					tokenAltern = True
					yield keySequence

	def yieldKeyFromByte(self,byte):
		for key in self.byteToKey[byte]:
			yield key

	def pressKey(self, key) :
		finger = self.keymap[key][1]
		key_pos = self.keymap[key][2:]
		hand = self.keymap[key][0]

		if hand == 'l' :
			self.leftHand.moveFinger(finger, key_pos)
			self.rightHand.resetFingers()
		elif hand == 'r' :
			self.rightHand.moveFinger(finger, key_pos)
			self.leftHand.resetFingers()
		else :
			self.leftHand.resetFingers()
			self.rightHand.resetFingers()

		self.hand = hand
		self.finger = finger
